const pgp = require("pg-promise")();

// Database configuration
const dbConfig = {
	host: "db",
	port: 5432,
	database: "users_db",
	user: "postgres",
	password: "pwd",
};

const db = pgp(dbConfig);

// Test database connection
db.connect()
	.then((obj) => {
		console.log("Database connection successful");
		obj.done();
	})
	.catch((error) => {
		console.log("ERROR:", error.message || error);
	});

module.exports = db;

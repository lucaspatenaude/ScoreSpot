const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
const db = require("../config/database");

// Redirect to the /login endpoint
app.get("/", (req, res) => {
	res.redirect("/home");
});

// Render login page for /login route
app.get("/login", (req, res) => {
	res.render("/");
});

// Trigger login form to check database for matching username and password
app.post("/login", async (req, res) => {
	try {
		// Check if username exists in DB
		const user = await db.oneOrNone(
			"SELECT * FROM users WHERE username = $1",
			req.body.username
		);

		if (!user) {
			// Redirect user to login screen if no user is found with the provided username
			return res.redirect("/register");
		}

		// Check if password from request matches with password in DB
		const match = await bcrypt.compare(req.body.password, user.password);

		// Check if match returns no data
		if (!match) {
			// Render the login page with the message parameter
			return res.render("/", { message: "Password does not match" });
		} else {
			// Save user information in the session variable
			req.session.user = user;
			req.session.save();

			// Redirect user to the home page
			res.redirect("/");
		}
	} catch (error) {
		// Direct user to login screen if no user is found with matching password
		res.redirect("/register");
	}
});

module.exports = loginRoutes;
